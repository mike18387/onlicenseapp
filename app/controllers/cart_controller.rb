class CartController < ApplicationController
  
  before_filter :authenticate_user!
  
  def add
    #get the id of a product 
    id = params[:id]
    #if the cart has already been created then the same cart will be used, otherwise a new cart is created. 
    
    if session[:cart]then 
      cart = session[:cart]
    else
      session[:cart]={}
      cart = session[:cart]
    
  end
  #if the product has already been added to the cart the value is incremented 
  #otherwise the value is set to 1.
  
  if cart[id] then
    cart[id] = cart[id]+1
  else 
    cart[id]=1
    end
    
  #redirect the cart display page 
  redirect_to :action => :index
end#end of add method

def clearCart
  #set session varialbe to nil and redirect 
  session[:cart] = nil
  redirect_to :action => :index
end

def remove
  id = params[:id]
  cart = session[:cart]
  cart.delete id
  
 redirect_to :action => :index
end



def index
  #if there is a cart, pass it to the page for display.
 #else pass an empty value
 if session[:cart]then 
   @cart = session[:cart]
 else
   @cart={}
 end

end
  


end

 


