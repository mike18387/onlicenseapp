class UsersController < ApplicationController
    
  def login
  end

  def admin_login
    session[:User_id] = 1
    session[:cart] = nil
    flash[:notice] = "Admin Login Successful!!"
    redirect_to :controller => :items
  end

  def logout
    session[:user_id] = nil
    session[:cart] = nil
    flash[:notice] = "You have successfully logged out!!"
    redirect_to :controller => :items
  end

end

  

